﻿using NUnit.Framework;

namespace ArrayValidation
{
    [TestFixture]
    public class MainProgramTest
    {
        [TestCase]
        public void TestArrayIsWilliamnized_ShouldReturnTrue ()
        { 
            // Arrange
            int [,] myArray = new int [3, 5] 
            { 
                { 2, 4, 8, 16, 32 }, 
                { 4, 8, 16, 32, 64 }, 
                { 8, 16, 32, 64, 128 } 
            };

            // Act
            var result = MainClass.ValidateArray (myArray);

            //Assert
            Assert.IsTrue (result, "Array passed the william test");
        }

        [TestCase]
        public void TestArrayDifferentSizeIsWilliamnized_ShouldReturnTrue ()
        {
        	// Arrange
        	int [,] myArray = new int [4, 7]
            { 
                { 1, 2, 4, 8, 16, 32, 64 }, 
                { 2, 4, 8, 16, 32, 64, 128 }, 
                { 4, 8, 16, 32, 64, 128, 256 }, 
                { 8, 16, 32, 64, 128, 256, 512 }
            };

        	// Act
        	var result = MainClass.ValidateArray (myArray);

        	//Assert
        	Assert.IsTrue (result, "Array 4x7 passed the william test");
        }

        [TestCase]
        public void TestNullArrayIsWilliamnized_ShouldReturnFalse ()
        {
        	// Arrange
        	int [,] myArray = null;

        	// Act
        	var result = MainClass.ValidateArray (myArray);

        	//Assert
        	Assert.IsFalse (result, "Array Null did NOT pass the william test");
        }

        [TestCase]
        public void TestArrayIsWilliamnized_ShouldReturnFalse ()
        {
        	// Arrange
        	int [,] myArray = new int [3, 5] { { 2, 4, 8, 16, 32 }, { 4, 8, 16, 32, 64 }, { 4, 8, 16, 32, 64 } };

        	// Act
        	var result = MainClass.ValidateArray (myArray);

        	//Assert
        	Assert.IsFalse (result, "Array 3x5 will not pass the william test");
        }
    }
}
