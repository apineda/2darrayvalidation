﻿using System;

namespace ArrayValidation
{
    class MainClass
    {
        public static void Main (string [] args)
        {
            int [,] myArray = new int [3, 5] { { 2, 4, 8, 16, 32 }, { 4, 8, 16, 32, 64 }, { 8, 16, 32, 64, 128 } };

            var result = ValidateArray (myArray);

            Console.ReadLine ();
        }


        /// <summary>
        /// Validates the array.
        ///    
        /// Given an 2D array like this
        /// 
        /// [2] [4] [8] [16] [32]
        /// [4] [8] [16] [32] [64]
        /// [8] [16] [32] [64] [128]
        /// 
        /// Function should returns true
        /// 
        /// </summary>
        /// <returns><c>true</c>, if array passed the validation, <c>false</c> otherwise.</returns>
        /// <param name="array">Array.</param>
        public static bool ValidateArray (int[,] array)
        {
            if (array == null)
                return false;

            int previousValue = 0;  
            int currentValue = 0;

            Console.WriteLine ("Running by rows ==>");

            for (int x = 0; x < array.GetLength(0); x++) 
            {
                for (int y = 0; y < array.GetLength(1); y++)
                {
                    //First iteration of the Y
                    if (y == 0)
                    {
                        previousValue = array [x, y];
                    }
                    else
                    {
                        currentValue = array [x,y];

                        var didPassed = IsPrevousNumberHalfOfCurrent (previousValue, currentValue);

                        LogInConsole (previousValue, currentValue, didPassed);

                        //If one doesn't meet the validation get out of the function.
                        if (!didPassed)
                            return false;

                        previousValue = currentValue;
                    }
                }
            }

            Console.WriteLine ("\nRunning by columns ==>");
            for (int y = 0; y <array.GetLength(1); y++) 
            {
                for (int x = 0; x <array.GetLength(0); x++)
                {
                    //First iteration of the Y
                    if (x == 0)
                    {
                        previousValue = array [x, y];
                    }
                    else
                    {
                        currentValue = array [x, y];

                        var didPassed = IsPrevousNumberHalfOfCurrent (previousValue, currentValue);

                        LogInConsole (previousValue, currentValue, didPassed);

                        //If one doesn't meet the validation get out of the function.
                        if (!didPassed)
                            return false;

                        previousValue = currentValue;
                    }
                }
            }

            //If code reached here is because it passed the validation made in IsPrevousNumberHalfOfCurrent()
            return true;
        }

        private static bool IsPrevousNumberHalfOfCurrent (int previous, int current)
        {
            return (previous * 2) == current;
        }


        private static void LogInConsole (int previous, int current, bool didPassed)
        { 
            System.Console.WriteLine 
                  ($"{previous} x 2 => {previous * 2} == {current} Equals? {didPassed}");
        }
    }
}
